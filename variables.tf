variable "vm_name" { default = "centos" }

variable "vm_memory" { default = "4096" }
variable "vm_cpu" { default = "4" }

variable "ext_addr" { default = "172.16.101" }

variable "int_addr" { default = "10.10.10" }

variable "vm_count" { default = 3 }

variable "ssh-pubkey" {
  ## FIXME
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDUq12VaIeWAlo3OBIu+AIKwbaWu/+RAeJrgdDtYxk4hSF+tlAuSMP84IfZefCET8VJEmK96OaYhWqyILfDYc/vBauv0hnWiBiUu6sU/P5BTnOyoBAq38rDzRSLm9EqQSJKAPma74kndAIHyTn/WyYrWAGksOUNItLjK7i8c7EOkrM1w1XZHuet88sfJ9mYBMDsssjKyJoGcCNH2HaGTANNhiCPvSuCB6YZmrP87iJWiqX4BJEaRG3kx2Qe636UvJfiHp5Ue8CD/hZIU09ksESuK2LWMMAwLwBq7R0V4+10VCFr0fUp+3yOlgMVlcGVTUAuUQJQQSl15Zv+6xlx+Uv5 jacob@DESKTOP-SQF182T"
}


